# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .process import Process
from .location import ProductCategoryLocation, Category


def register():
    Pool.register(
        ProductCategoryLocation,
        Category,
        module='business_process', type_='model',
        depends=['stock_product_category_location']),
    Pool.register(
        Process,
        module='business_process', type_='model')
